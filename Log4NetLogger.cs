﻿using log4net;
using System;

namespace Acrotech.PortableLogAdapter.NLogAdapter
{
    /// <summary>
    /// This class wraps logging to an NLog logger
    /// </summary>
    public class Log4NetLogger : ILogger
    {
        /// <summary>
        /// Creates a new NLog logger wrapper
        /// </summary>
        /// <param name="logger">Base NLog logger to funnel logging to</param>
        public Log4NetLogger(ILog logger)
        {
            Logger = logger;
        }

        /// <summary>
        /// Base NLog Logger associated with this NLogLogger
        /// </summary>
        public ILog Logger { get; private set; }

        /// <inheritdoc/>
        public string Name { get { return Logger.Logger.Name; } }

        /// <inheritdoc/>
        public void Log(LogLevel level, string format, params object[] args)
        {
            GetLogFunction(Logger, level).Invoke(format.FormatSafe(args));
        }

        /// <inheritdoc/>
        public void Log(LogLevel level, Func<string> messageCreator)
        {
            if (IsEnabled(level))
            {
                GetLogFunction(Logger, level).Invoke(messageCreator());
            }
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, string format, params object[] args)
        {
            GetLogExceptionFunction(Logger, level).Invoke(format.FormatSafe(args), exception);
        }

        /// <inheritdoc/>
        public void LogException(LogLevel level, Exception exception, Func<string> messageCreator)
        {
            if (IsEnabled(level))
            {
                GetLogExceptionFunction(Logger, level).Invoke(messageCreator(), exception);
            }
        }

        /// <inheritdoc/>
        public bool IsEnabled(LogLevel level)
        {
            return Logger.Logger.IsEnabledFor(Convert(level));
        }

        /// <summary>
        /// Converts a PortableLogAdapter level to an NLog level
        /// </summary>
        /// <param name="level">PortableLogAdapter level to convert</param>
        /// <returns>Equivalent NLog level</returns>
        public static log4net.Core.Level Convert(LogLevel level)
        {
            var result = log4net.Core.Level.Debug;

            switch (level)
            {
                case LogLevel.Fatal:
                    result = log4net.Core.Level.Fatal;
                    break;
                case LogLevel.Error:
                    result = log4net.Core.Level.Error;
                    break;
                case LogLevel.Warn:
                    result = log4net.Core.Level.Warn;
                    break;
                case LogLevel.Info:
                    result = log4net.Core.Level.Info;
                    break;
            }

            return result;
        }

        #region Log Function Getters

        public static Action<object> GetLogFunction(ILog logger, LogLevel level)
        {
            Action<object> action = null;

            switch (level)
            {
                case LogLevel.Fatal:
                    action = logger.Fatal;
                    break;
                case LogLevel.Error:
                    action = logger.Error;
                    break;
                case LogLevel.Warn:
                    action = logger.Warn;
                    break;
                case LogLevel.Info:
                    action = logger.Info;
                    break;
                case LogLevel.Debug:
                    action = logger.Debug;
                    break;
                case LogLevel.Trace:
                    action = logger.Debug;
                    break;
            }

            return action;
        }

        public static Action<object, Exception> GetLogExceptionFunction(ILog logger, LogLevel level)
        {
            Action<object, Exception> action = null;

            switch (level)
            {
                case LogLevel.Fatal:
                    action = logger.Fatal;
                    break;
                case LogLevel.Error:
                    action = logger.Error;
                    break;
                case LogLevel.Warn:
                    action = logger.Warn;
                    break;
                case LogLevel.Info:
                    action = logger.Info;
                    break;
                case LogLevel.Debug:
                    action = logger.Debug;
                    break;
                case LogLevel.Trace:
                    action = logger.Debug;
                    break;
            }

            return action;
        }

        #endregion
    }
}
