﻿# README #

This library is intended to provide log4net support for platform targets that use the PortableLogAdapter framework.

### What is this repository for? ###

* Anyone who wants to use log4net in their cross-platform (or PCL-based) application or library.

### How do I get set up? ###

0. Add this library via NuGet to your platform target
0. Add the ProtableLogAdapter library to your non-platform target(s)
0. Use dependency injection (with ILogManager) or create Singleton accessors to acquire ILogger instances

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.0.0.0 ####

* Initial Release
