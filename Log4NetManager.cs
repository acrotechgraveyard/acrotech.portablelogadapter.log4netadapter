﻿using log4net;
using System;
using System.Collections.Generic;
namespace Acrotech.PortableLogAdapter.NLogAdapter
{
    /// <summary>
    /// This class creates NLog loggers
    /// </summary>
    public class Log4NetManager : ILogManager
    {
        /// <summary>
        /// This is the default (and only) NLogManager instance
        /// </summary>
        public static readonly Log4NetManager Default = new Log4NetManager();

        private Log4NetManager()
        {
        }

        /// <summary>
        /// Creates a new NLogLogger with the provided <paramref name="name"/>
        /// </summary>
        /// <param name="name">Logger name</param>
        /// <returns>The NLogLogger</returns>
        public ILogger GetLogger(string name)
        {
            return new Log4NetLogger(log4net.LogManager.GetLogger(name));
        }
    }
}
